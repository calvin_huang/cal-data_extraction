import sys
import hvac
import os

sys.path.insert(0, '/home/ubuntu')
from vault_config import reader

client = hvac.Client(
    url='https://vault-hq.neuron.sg',
    token=reader.VAULT_TOKEN)
"""
from neuron_data_utils.db_access import Db_access
client=Db_access().get_vault_client()
"""
redshift_editor_auth = client.secrets.kv.v2.read_secret_version(
    mount_point='redshift-editor', path='database-editor')


class Config:
    redshift = 'redshift+psycopg2://{}:{}@redshift-cluster-1.cqvneaepqxlh.ap-southeast-1.redshift.amazonaws.com:5439/neuron_pro'.format(
        redshift_editor_auth['data']['data']['redshift.editor.username'],
        redshift_editor_auth['data']['data']['redshift.editor.password'])
    bucket = 'neuron-prod-data'
    path = 'db/'


class ProductionConfig(Config):
    schema = ['neuron_australia', 'neuron_newzealand', 'neuron_gb', 'neuron_canada', 'neuron_korea']

    battery_city_config = {
        'MEL': 0.2,
    }
    battery_city_config_default = 0.3

    status_columns = """(city, created_at, in_station_parking, in_station_out_station, in_station_pickup,
                        in_station_staging, in_reservation, no_riding, outside_service, admin_lock, in_trip, 
                        in_stock, in_repair, in_warehouse, missing, beyond_repair, rebalancing, rebalancing_and_available,
                        in_trip_and_available, no_riding_and_available,outside_service_and_available,
                        admin_lock_and_available, in_reservation_and_available,in_station_and_available,
                        in_station_ops_only, expansion, storage, in_station_unavailable_area, 
                        in_station_unavailable_area_and_available ,vehicle_type,zone)"""

    status_schema = """CREATE TABLE {}.city_status_count (
                                city VARCHAR(32),
                                created_at TIMESTAMP,
                                in_station_parking BIGINT, 
                                in_station_out_station BIGINT, 
                                in_station_pickup BIGINT,
                                in_station_staging BIGINT,
                                in_reservation BIGINT, 
                                no_riding BIGINT, 
                                outside_service BIGINT, 
                                admin_lock BIGINT, 
                                in_trip BIGINT, 
                                in_stock BIGINT, 
                                in_repair BIGINT, 
                                in_warehouse BIGINT, 
                                missing BIGINT, 
                                beyond_repair BIGINT, 
                                rebalancing BIGINT,
                                rebalancing_and_available BIGINT,
                                in_trip_and_available BIGINT,
                                no_riding_and_available BIGINT,
                                outside_service_and_available BIGINT,
                                admin_lock_and_available BIGINT,
                                in_reservation_and_available BIGINT,
                                in_station_and_available BIGINT,
                                in_station_ops_only BIGINT,
                                vehicle_type VARCHAR(30),
                                ZONE VARCHAR(32),
                                expansion BIGINT, 
                                storage BIGINT,  
                                in_station_unavailable_area BIGINT, 
                                in_station_unavailable_area_and_available BIGINT
                             ) """
