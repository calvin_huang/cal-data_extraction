import psycopg2
import pandas as pd
import datetime
import numpy as np
import pymysql
import boto3
import sqlalchemy as sa
from config_2 import Config, ProductionConfig
import json
from io import StringIO
import logging
import sys
import hvac
import os

sys.path.insert(0, '/home/ubuntu')

from vault_config import reader

client = hvac.Client(
    url='https://vault-hq.neuron.sg',
    token=reader.VAULT_TOKEN)
'''
from neuron_data_utils.db_access import Db_access

client=Db_access().get_vault_client()
'''
mysql_reader_auth = client.secrets.kv.v2.read_secret_version(
    mount_point='mysql-reader', path='database-reader')

iam_auth = client.secrets.kv.v2.read_secret_version(
    mount_point='neuron-data', path='iam-role')

# Establishing Logging
logging.basicConfig(filename="city_scooter_status_logs.txt",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=20)
logging.info("=======================================================================================================")
logging.info("")
logging.info("")

logging.info("------ This run occured at: " + str(datetime.datetime.now()) + "------")

app = ProductionConfig

# S3 Paths
session = boto3.Session(profile_name='dev')
s3 = session.client('s3', region_name='ap-southeast-1')

# Creating scooter/gps table if it does not exists
redshift = sa.create_engine(app.redshift, execution_options={'autocommit': True})

# %%


'''
This section is a mapping of the scooter statuses and station types to the fields for status count
'''


def category(row):
    if row['status'] == 'IN_WAREHOUSE':
        return ('in_warehouse')
    elif row['status'] == 'IN_REPAIR':
        return ('in_repair')
    elif row['status'] == 'IN_STOCK':
        return ('in_stock')
    elif row['status'] == 'MISSING':
        return ('missing')
    elif row['status'] == 'EXPANSION':
        return ('expansion')
    elif row['status'] == 'STORAGE':
        return ('storage')
    elif row['status'] == 'BEYOND_REPAIR':
        return ('beyond_repair')
    elif row['status'] == 'REBALANCING':
        return ('rebalancing')
    elif row['status'] == 'IN_TRIP':
        return ('in_trip')
    elif row['status'] == 'NO_RIDING':
        return ('no_riding')
    elif row['status'] == 'OUTSIDE_SERVICE':
        return ('outside_service')
    elif row['status'] == 'ADMIN_LOCK':
        return ('admin_lock')
    elif row['status'] == 'IN_RESERVATION':
        return ('in_reservation')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and row['station_type'] in [
        'PARKING', 'DEPLOYMENT']:
        return ('in_station_parking')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and row[
        'station_type'] == 'WAREHOUSE':
        # this is to account for legacy logic when we did not have the IN_WAREHOUSE status
        # artificially parking this into the in_station_parking category as we intend to abide by the IN_STATION logic going forward
        return ('in_station_parking')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and row[
        'station_type'] == 'PICKUP':
        return ('in_station_pickup')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and row[
        'station_type'] == 'STAGING':
        return ('in_station_staging')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and row[
        'station_type'] == 'NO_STATION':
        return ('in_station_out_station')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and row['station_type'] in [
        'OPS_ONLY', 'MAINTENANCE']:
        return ('in_station_ops_only')
    elif row['status'] == 'IN_STATION' and (row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS '):
        return ('in_station_other_station')
    elif row['status'] == 'IN_STATION' and row['sub_status'] == 'UNAVAILABLE_AREA':
        return ('in_station_unavailable_area')
    else:
        return ('other')


def batteryStatus(row):
    if row['remaining_battery'] >= 0.5:
        return ('green')
    else:
        battery_yellow_level = app.battery_city_config.get(row['city'], app.battery_city_config_default)
        if row['remaining_battery'] >= battery_yellow_level:
            return ('yellow')
        else:
            return ('red')


def availabilityCategory(row):
    if row['status'] == 'REBALANCING' and (row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('rebalancing_and_available')
    elif row['status'] == 'IN_TRIP' and (row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('in_trip_and_available')
    elif row['status'] == 'NO_RIDING' and (row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('no_riding_and_available')
    elif row['status'] == 'OUTSIDE_SERVICE' and (
            row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('outside_service_and_available')
    elif row['status'] == 'ADMIN_LOCK' and (
            row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('admin_lock_and_available')
    elif row['status'] == 'IN_RESERVATION' and (
            row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('in_reservation_and_available')
    elif row['status'] == 'IN_STATION' and (
            row['sub_status'] == 'AVAILABLE' or row['sub_status'] == 'NO_SUB_STATUS ') and (
            row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('in_station_and_available')
    elif row['status'] == 'IN_STATION' and row['sub_status'] == 'UNAVAILABLE_AREA' and (
            row['remaining_battery'] == 'green' or row['remaining_battery'] == 'yellow'):
        return ('in_station_unavailable_area_and_available')
    else:
        return (None)


'''
This is where we loop through the schemas to perform the groupings and countings
'''
for schema in app.schema:
    # get data
    if schema == 'neuron_gb':
        conn = pymysql.connect("gb-pro-reader.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                               mysql_reader_auth['data']['data']['rds.gb.reader.username'],
                               mysql_reader_auth['data']['data']['rds.gb.reader.password'], schema)
    elif schema == 'neuron_korea':
        conn = pymysql.connect("kr-pro-reader.c7ckiqj52mxh.ap-northeast-2.rds.amazonaws.com",
                               mysql_reader_auth['data']['data']['rds.kr.reader.username'],
                               mysql_reader_auth['data']['data']['rds.kr.reader.password'], schema)
    elif schema in ['neuron_australia', 'neuron_newzealand']:
        conn = pymysql.connect("neuron-pro-au-read.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                               mysql_reader_auth['data']['data']['rds.au.reader.username'],
                               mysql_reader_auth['data']['data']['rds.au.reader.password'], schema)
    elif schema == 'neuron_canada':
        conn = pymysql.connect("ca-pro-slave.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                               mysql_reader_auth['data']['data']['rds.ca.reader.username'],
                               mysql_reader_auth['data']['data']['rds.ca.reader.password'], schema)
    else:
        raise Exception('no connection found for ' + schema)


    def query_scooter_inventory():
        sql = """
                SELECT
                    scooter_inventory.city,
                    scooter_inventory.id,
                    scooter_inventory.status,
                    scooter_inventory.sub_status,
                    scooter_inventory.created_at,
                    scooter_inventory.updated_at,
                    scooter_inventory.type vehicle_type,
                    CASE
                        WHEN scooter_inventory.zone IS NULL THEN 'None'
                        ELSE scooter_inventory.zone
                    END zone,
                    station.name,
                    station.type station_type,
                    gps_inventory.remaining_battery
                FROM {schema}.scooter_inventory
                LEFT JOIN {schema}.station ON scooter_inventory.station_id=station.id
                LEFT JOIN {schema}.gps_inventory ON scooter_inventory.imei=gps_inventory.imei
              """
        cty = pd.read_sql(sql.format(schema=schema), conn)
        return cty


    scooter_inventory = query_scooter_inventory()
    # print(scooter_inventory)
    scooter_inventory['station_type'] = scooter_inventory['station_type'].fillna('NO_STATION')
    scooter_inventory['sub_status'] = scooter_inventory['sub_status'].fillna('NO_SUB_STATUS')
    scooter_inventory['remaining_battery'] = scooter_inventory.apply(batteryStatus, axis=1)

    # calculation for statuses
    status_count = scooter_inventory.groupby(['vehicle_type', 'city', 'zone', 'status', 'station_type', 'sub_status'])[
        'id'].count().reset_index()
    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    # print(status_count)
    # print(status_count[['city', 'zone', 'status', 'station_type','sub_status', 'id']])
    status_count['city_status'] = None
    status_count['city_status'] = status_count.apply(category, axis=1)
    status_count = pd.pivot_table(status_count, values='id', index=['city', 'zone', 'vehicle_type'],
                                  columns=['city_status'], aggfunc=np.sum).fillna(0).astype(int).reset_index()

    # calculation for availability
    availability_count = \
    scooter_inventory.groupby(['vehicle_type', 'city', 'zone', 'status', 'remaining_battery', 'sub_status'])[
        'id'].count().reset_index()
    availability_count['city_status'] = None
    availability_count['city_status'] = availability_count.apply(availabilityCategory, axis=1)
    availability_count = pd.pivot_table(availability_count, values='id', index=['city', 'zone', 'vehicle_type'],
                                        columns=['city_status'], aggfunc=np.sum).fillna(0).astype(int).reset_index()

    # merging of the two tables
    status_and_availability_count = status_count.merge(availability_count, how='left',
                                                       on=['city', 'zone', 'vehicle_type'])
    if 'in_station_parking' not in status_and_availability_count:
        status_and_availability_count['in_station_parking'] = np.nan
    if 'in_station_out_station' not in status_and_availability_count:
        status_and_availability_count['in_station_out_station'] = np.nan
    if 'in_station_pickup' not in status_and_availability_count:
        status_and_availability_count['in_station_pickup'] = np.nan
    if 'in_station_staging' not in status_and_availability_count:
        status_and_availability_count['in_station_staging'] = np.nan
    if 'in_station_ops_only' not in status_and_availability_count:
        status_and_availability_count['in_station_ops_only'] = np.nan
    if 'in_station_other_station' not in status_and_availability_count:
        status_and_availability_count['in_station_other_station'] = np.nan

    status_and_availability_count['in_station_circulating'] = status_and_availability_count[
                                                                  'in_station_parking'].fillna(0) + \
                                                              status_and_availability_count[
                                                                  'in_station_out_station'].fillna(0) + \
                                                              status_and_availability_count['in_station_pickup'].fillna(
                                                                  0) + \
                                                              status_and_availability_count[
                                                                  'in_station_staging'].fillna(0) + \
                                                              status_and_availability_count[
                                                                  'in_station_ops_only'].fillna(0) + \
                                                              status_and_availability_count[
                                                                  'in_station_other_station'].fillna(0)

    status_and_availability_count_df = pd.DataFrame(columns=['city', 'created_at',
                                                             'in_station_parking', 'in_station_out_station',
                                                             'in_station_pickup', 'in_station_staging',
                                                             'in_reservation', 'no_riding', 'outside_service',
                                                             'admin_lock', 'in_trip',
                                                             'in_stock', 'in_repair', 'in_warehouse', 'missing',
                                                             'beyond_repair', 'rebalancing',
                                                             'rebalancing_and_available', 'in_trip_and_available',
                                                             'no_riding_and_available',
                                                             'outside_service_and_available',
                                                             'admin_lock_and_available', 'in_reservation_and_available',
                                                             'in_station_and_available', 'in_station_ops_only',
                                                             'expansion',
                                                             'storage', 'in_station_unavailable_area',
                                                             'in_station_unavailable_area_and_available',
                                                             'in_station_other_station', 'in_station_circulating',
                                                             'vehicle_type', 'zone'
                                                             ],
                                                    data=status_and_availability_count)

    status_and_availability_count_df['created_at'] = datetime.datetime.now().replace(microsecond=0)
    status_and_availability_count_df = status_and_availability_count_df.fillna(0)

    status_and_availability_count_df.iloc[:, 2:-2] = status_and_availability_count_df.iloc[:, 2:-2].astype(int)

    # %%
    # Creating scooter status table if it does not exists
    if not redshift.dialect.has_table(redshift, 'city_status_count', schema=schema):
        print("Table status count does not exist. Creating table")
        logging.info("Table status count does not exist. Creating table")
        redshift.execute(app.status_schema.format(schema))

    # %%
    # Uploading to S3
    upload_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")  #
    csv_buffer = StringIO()
    status_and_availability_count_df.to_csv(csv_buffer, index=False)
    status_key = app.path + schema + '/city_scooter_status_count/' + str(upload_time) + '.csv'
    response = s3.put_object(Body=csv_buffer.getvalue(),
                             Bucket=app.bucket,
                             Key=status_key)
    logging.info('status_key:' + status_key)

    # %%
    # Load into redshift
    location = "s3://" + app.bucket + "/" + status_key

    copy_command = "copy {}.city_status_count {} from '{}' iam_role '{}' csv IGNOREHEADER 1 timeformat 'auto' FILLRECORD;".format(
        schema, app.status_columns, location, iam_auth['data']['data']['redshift.role'])
    logging.info(copy_command)

    error = redshift.execute(copy_command)
    logging.info('copy command response: ' + str(error))

    logging.info("Completed loading City Status Count")