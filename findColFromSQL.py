import pandas as pd


txt = 'foundSQL.txt'
ddl_csv = 'query-All_DDLs.csv'
schemas = ['neuron_australia', 'neuron_newzealand', 'neuron_gb', 'neuron_canada']
table = 'city_status_count'
csv_data = pd.read_csv(ddl_csv)
gb = csv_data.groupby(['schemaname', 'tablename'])

with open(txt, 'w') as f:
    for schema in schemas:
        ddl = gb.get_group((schema, table))['ddl'].values
        sql = '\n'.join(ddl)
        f.write(sql)
        f.write('\n\n\n')
        print('{}.{} sql created.'.format(schema, table))
