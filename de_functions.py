import hvac
import psycopg2
import pandas as pd
import json


def dataAccess(token, dbname, query, output=None):
    # copy from https://bitbucket.org/neuroncn/cs-tracking/src/master/cs_tracking.py
    client = hvac.Client(url='https://vault-hq.neuron.sg',
                         token=token)
    redshift_reader_auth = client.secrets.kv.v2.read_secret_version(mount_point='redshift-reader',
                                                                    path='database-reader')
    red_conn = psycopg2.connect(dbname=dbname,
                                host='redshift-cluster-1.cqvneaepqxlh.ap-southeast-1.redshift.amazonaws.com',
                                port='5439',
                                user=redshift_reader_auth['data']['data']['redshift.reader.username'],
                                password=redshift_reader_auth['data']['data']['redshift.reader.password'])
    # get csv for raw_dara
    tickets = pd.read_sql(query, red_conn)
    if output is not None:
        tickets.to_csv(output, index=False)
        print("Query output generated in CSV <{}>".format(output))
    return tickets


def csv2json4structure(csv_file, json_file):
    # get dataframe
    csv_data = pd.read_csv(csv_file)
    # get dict
    df = csv_data
    df = df.reset_index()  # make sure indexes pair with number of rows
    dic = {}
    for index, row in df.iterrows():
        schema = row['table_schema']
        table = row['table_name']
        column = row['column_name']
        # data_type = row['data_type']
        # length = row['character_maximum_length']
        if schema not in dic:
            dic[schema] = {}
        if table not in dic[schema]:
            dic[schema][table] = {}
        if column not in dic[schema][table]:
            dic[schema][table][column] = {}
            # dic[schema][table][column] = {'data_type': data_type, 'character_maximum_length': length}
    # get json
    json_object = json.dumps(dic, indent=4)
    with open(json_file, "w") as f:
        f.write(json_object)


def readJson(json_file):
    with open(json_file, 'r') as f:
        data = json.load(f)
    for schema in data:
        print('Schema:', schema, '->', len(data[schema].keys()), 'tables')
    return data


def writeQuery4DDL(schema, table):
    query = "select ddl from v_generate_tbl_ddl where schemaname = '%s' and tablename = '%s';" % (schema, table)
    return query


def writeDDLs_test(token, dbname, json_file, ddl_txt):
    with open(json_file, 'r') as f:
        d = json.load(f)
    with open(ddl_txt, 'w') as f:
        for schema in d:
            for table in d[schema]:
                query = writeQuery4DDL(schema, table)
                ddl = dataAccess(token, dbname, query, output=None)
                for value in ddl.values:
                    f.write(value[0])


def writeDDLs(json_file, csv_file, txt):
    with open(json_file, 'r') as f:
        json_data = json.load(f)
    csv_data = pd.read_csv(csv_file)
    gb = csv_data.groupby(['schemaname', 'tablename'])
    with open(txt, 'w') as f:
        for schema in json_data:
            f.write('CREATE SCHEMA IF NOT EXISTS %s;\n' % schema)
            for table in json_data[schema]:
                ddl = gb.get_group((schema, table))['ddl'].values
                sql = '\n'.join(ddl)
                f.write(sql)
                f.write('\n\n\n')
    print('DDLs are written in <%s>.' % txt)
