import de_functions as func


def getRawData():
    # neuron_country
    my_token = 's.eeuWBR9CnptAMqdP7X8x1N2S'
    my_db = 'neuron_pro'
    my_query = '''
    select table_schema, table_name, column_name, data_type, character_maximum_length
    from information_schema.columns
    where table_schema = 'neuron_gb' 
    or table_schema = 'neuron_australia'
    or table_schema = 'neuron_newzealand'
    or table_schema = 'neuron_canada'
    or table_schema = 'neuron_user'
    '''
    my_csv = 'query-pro-neuron_qa.csv'
    my_json = 'neuron_qa.json'
    func.dataAccess(my_token, my_db, my_query, my_csv)
    func.csv2json4structure(my_csv, my_json)


def getDDLs_test():
    # slow due to multiple times of connections to database
    my_token = 's.eeuWBR9CnptAMqdP7X8x1N2S'
    my_db = 'neuron_pro'
    json_file = 'neuron_qa.json'
    ddl_txt = 'test.txt'
    func.writeDDLs(my_token, my_db, json_file, ddl_txt)


def getRawDDLs():
    my_token = 's.eeuWBR9CnptAMqdP7X8x1N2S'
    my_db = 'neuron_pro'
    my_query = """
    select schemaname, tablename, ddl
    from v_generate_tbl_ddl 
    where schemaname = 'neuron_gb' 
    or schemaname = 'neuron_australia'
    or schemaname = 'neuron_newzealand'
    or schemaname = 'neuron_canada'
    or schemaname = 'neuron_user'
    """
    output = 'query-All_DDLs.csv'
    func.dataAccess(my_token, my_db, my_query, output=output)


def getDDLs():
    csv_file = 'query-All_DDLs.csv'
    json_file = 'neuron_qa.json'
    txt = 'neuron_qa-sql.txt'
    func.writeDDLs(json_file, csv_file, txt)


getRawData()
getRawDDLs()
getDDLs()
